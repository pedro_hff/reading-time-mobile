import 'package:flutter/material.dart';
import 'package:mobile/constants/custom_icon.dart';

class TimelinePageItemStateModel {
  String runId;
  String bookUrl;
  String bookName;
  String totalTimeCounter;
  String pageStopped;
  String lastVisit;
  String daysSinceStarted;
  String pagesPerMinute;
  String percentage;
  _TimelinePageItemStateRowModel _bookNameRow;
  _TimelinePageItemStateRowModel _totalTimeCounterRow;
  _TimelinePageItemStateRowModel _pageStoppedRow;
  _TimelinePageItemStateRowModel _lastVisitRow;
  _TimelinePageItemStateRowModel _daysSinceStarted;
  _TimelinePageItemStateRowModel _pagesPerMinute;
  _TimelinePageItemStateRowModel _percentage;


  TimelinePageItemStateModel(this.runId, this.bookUrl, this.bookName,
      this.totalTimeCounter, this.pageStopped, this.lastVisit, this.daysSinceStarted, this.pagesPerMinute,
      this.percentage);

  Widget buildWidget() {
    _bookNameRow = _TimelinePageItemStateRowModel(CustomIcon.BOOK, 'Book name', bookName);
    _totalTimeCounterRow = _TimelinePageItemStateRowModel(CustomIcon.LOGO, 'Total time counter', totalTimeCounter);
    _pageStoppedRow = _TimelinePageItemStateRowModel(CustomIcon.LOGO, 'Page stopped', pageStopped);
    _lastVisitRow = _TimelinePageItemStateRowModel(CustomIcon.CALENDAR_EMPTY, 'Last visit', lastVisit);
    _daysSinceStarted = _TimelinePageItemStateRowModel(CustomIcon.CALENDAR_FILLED, 'Days since started', daysSinceStarted);
    _pagesPerMinute = _TimelinePageItemStateRowModel(CustomIcon.LOGO, "Pages per minute", pagesPerMinute);
    _percentage = _TimelinePageItemStateRowModel(CustomIcon.LOGO, "Percentage read", percentage);

    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Card(
          clipBehavior: Clip.hardEdge,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0)
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.green,
                      image: DecorationImage(
                          image: NetworkImage(
                            bookUrl,
                          ),
                        fit: BoxFit.fill
                      )
                    ),
                  )
              ),
              Container(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Table(
                    columnWidths: {
                      0: FixedColumnWidth(20.0),
                      1: FixedColumnWidth(125.0),
                      2: FlexColumnWidth()
                    },
                    children: [
                      _bookNameRow.buildRow(),
                      _totalTimeCounterRow.buildRow(),
                      _pageStoppedRow.buildRow(),
                      _lastVisitRow.buildRow(),
                      _daysSinceStarted.buildRow(),
                      _pagesPerMinute.buildRow(),
                      _percentage.buildRow()
                    ],
                  ),
                )
              )
            ],
          )
        ),
      ),
    );
  }
}

class _TimelinePageItemStateRowModel {
  String iconPath;
  String description;
  String value;

  _TimelinePageItemStateRowModel(this.iconPath, this.description, this.value);

  TableRow buildRow() {
    return TableRow(
      children: [

        Container(
          width: 14.0,
          height: 14.0,
          padding: EdgeInsets.only(right: 4.0),
          child: ImageIcon(
            new AssetImage(this.iconPath),
            size: 12.0,
            color: Colors.green,
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          width: 40.0,
          child: Text(
            this.description,
            textAlign: TextAlign.start,
            maxLines: 1,
            style: TextStyle(
              fontWeight: FontWeight.w100,
              color: Colors.green
            ),
          ),
        ),
        Text(
          this.value,
          textAlign: TextAlign.end,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        )
      ]
    );
  }
}