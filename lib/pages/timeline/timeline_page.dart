import 'package:flutter/material.dart';
import 'package:mobile/pages/timeline/timeline_page_item.dart';

class TimelinePage extends StatelessWidget {
  TimelinePage({Key key}): super(key:key);

  Widget _buildPageView() {
    return PageView(
      children: <Widget>[
        TimelinePageItemStateModel("--", "https://d188rgcu4zozwl.cloudfront.net/content/B01MYXX8K6/images/cover.jpg", "O universo numa casca de noz", "00:00:00", "0", "10/10/2018", "4", "0.0", "0%").buildWidget(),
        TimelinePageItemStateModel("--", "https://www.darksidebooks.com.br/wp-content/uploads/2017/10/lovecraft-cosmic-capa-catalogo.png", "H.P. Lovecraft: Medo Clássico Vol. 1 - Cosmic Edition", "00:00:00", "0", "10/10/2018", "33", "33.5", "30%").buildWidget(),
        TimelinePageItemStateModel("--", "https://images-na.ssl-images-amazon.com/images/I/714x8zlcPvL.jpg", "Sandman - Edição Definitiva - Vol. 4", "00:00:00", "9999", "10/10/2018", "1", "2.0", "0%").buildWidget(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildPageView(),
    );
  }


}