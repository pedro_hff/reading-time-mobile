import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobile/main_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.green,
      statusBarIconBrightness: Brightness.light
    ));
    return new MaterialApp(
      title: 'Reading Time',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.green,
        primaryColor: Colors.green.shade300,
        accentColor: Colors.tealAccent.shade50,
        primaryColorDark: Colors.green.shade600
      ),
      home: MainPage(),
    );
  }
}