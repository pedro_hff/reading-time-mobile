import 'package:flutter/material.dart';
import 'package:mobile/pages/timeline/timeline_page.dart';
import 'package:mobile/widgets/chronometer_controller.dart';


class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => new _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Widget _bodyLayout = TimelinePage();
  BottomNavigationBar bottomNavigationBar;
  List<BottomNavigationBarItem> bottomNavigationBarItems;
  int currentIndex = 1;
  String _currentBookName = 'Book name test loong name';
  ChronometerController _chronometerController;
  bool visible = false;

  void _onLayoutSelected(Widget selection, int index) {
    setState(() {
      currentIndex = index;
      _bodyLayout = selection;
    });
  }

  void _onSelectItem(int index) {
    setState(() {
      visible = !visible;
    });
    switch (index) {
      case 0:
        _onLayoutSelected(TimelinePage(), index);
        break;
      case 1:
        _onLayoutSelected(TimelinePage(), index);
        break;
      case 2:
        _onLayoutSelected(TimelinePage(), index);
        break;
    }
  }

  void _buildItem(
      {String iconPath, String description, int itemIndex, int currentIndex}) {
    Color navBarItemColor = itemIndex == currentIndex ? Colors.green.shade300 : Colors.black26;
    BottomNavigationBarItem newItem = BottomNavigationBarItem(
      icon: new ImageIcon(
          new AssetImage(iconPath),
          color: navBarItemColor,
      ),
      title: Text(
      description,
        style: TextStyle(
          color: navBarItemColor,
        ),
      ),
    );

    bottomNavigationBarItems.add(newItem);
  }

  _buildBottomNavbarItems(int index) {
    _buildItem(
      iconPath: 'assets/books.png',
      description: 'Library',
      itemIndex: 0,
      currentIndex: index
    );
    _buildItem(
      iconPath: 'assets/logo-icon/logo-icon.png',
      description: 'Timeline',
      itemIndex: 1,
      currentIndex: index
    );
    _buildItem(
      iconPath: 'assets/chart-bar.png',
      description: 'Stats',
      itemIndex: 2,
      currentIndex: index
    );
  }

  Widget _buildBottomNavigationBar() {
    _buildBottomNavbarItems(currentIndex);
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: bottomNavigationBarItems,
      onTap: _onSelectItem,
    );
  }

  @override
  Widget build(BuildContext context) {
    bottomNavigationBarItems = new List();
    _chronometerController = ChronometerController(
      bookName: _currentBookName,
      visibility: visible
    );
    return Scaffold(
      body: _bodyLayout,
      bottomNavigationBar: _buildBottomNavigationBar(),
      floatingActionButton: _chronometerController,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}