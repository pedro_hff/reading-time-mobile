
class CustomIcon {
  static const LOGO = 'assets/logo-icon/logo-icon.png';
  static const BOOK = 'assets/book-open.png';
  static const LIBRARY = 'assets/books.png';
  static const CALENDAR_EMPTY = 'assets/calendar.png';
  static const CALENDAR_FILLED = 'assets/calendar-alt.png';
  static const CALENDAR_DONE = 'assets/calendar-check.png';
  static const STATISTICS = 'assets/chart-bar.png';
  static const CLOCK = 'assets/clock.png';
}