import 'package:flutter/material.dart';
import 'dart:async';

/**
 * Source: https://github.com/bizz84/stopwatch-flutter/blob/master/lib/timer_page.dart
 */

class ElapsedTime {
  final int seconds;
  final int minutes;
  final int hours;

  ElapsedTime({
    this.seconds,
    this.minutes,
    this.hours
  });
}

class Dependencies {

  final List<ValueChanged<ElapsedTime>> timerListeners = <ValueChanged<ElapsedTime>>[];
  final TextStyle textStyle = const TextStyle(fontSize: 18.0);
  final Stopwatch stopwatch = new Stopwatch();
  final int timerMillisecondsRefreshRate = 1000;
}


class TimerText extends StatefulWidget {
  TimerText({this.dependencies});
  final Dependencies dependencies;

  TimerTextState createState() => new TimerTextState(dependencies: dependencies);
}

class TimerTextState extends State<TimerText> {
  TimerTextState({this.dependencies});
  final Dependencies dependencies;
  Timer timer;
  int milliseconds;

  @override
  void initState() {
    //TODO: load from memory database the elapsed time and set it
    timer = new Timer.periodic(new Duration(milliseconds: dependencies.timerMillisecondsRefreshRate), callback);
    super.initState();
  }

  @override
  void dispose() {
    //TODO: save to memory database the elapsed time
    timer?.cancel();
    timer = null;
    super.dispose();
  }

  void callback(Timer timer) {
    if (milliseconds != dependencies.stopwatch.elapsedMilliseconds) {
      milliseconds = dependencies.stopwatch.elapsedMilliseconds;
      final int seconds = (milliseconds / 1000).truncate();
      final int minutes = (seconds / 60).truncate();
      final int hours = (minutes / 60).truncate();
      final ElapsedTime elapsedTime = new ElapsedTime(
        seconds: seconds,
        minutes: minutes,
        hours: hours
      );
      for (final listener in dependencies.timerListeners) {
        listener(elapsedTime);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new ChrometerView(dependencies: dependencies);
  }
}

class ChrometerView extends StatefulWidget {
  ChrometerView({this.dependencies});
  final Dependencies dependencies;

  ChrometerViewState createState() => new ChrometerViewState(dependencies: dependencies);
}

class ChrometerViewState extends State<ChrometerView> {
  ChrometerViewState({this.dependencies});
  final Dependencies dependencies;

  int hours = 0;
  int minutes = 0;
  int seconds = 0;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.hours != hours || elapsed.minutes != minutes || elapsed.seconds != seconds) {
      setState(() {
        hours = elapsed.hours;
        minutes = elapsed.minutes;
        seconds = elapsed.seconds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String hoursStr = (hours % 24).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return new Text('$hoursStr:$minutesStr:$secondsStr', style: dependencies.textStyle, textAlign: TextAlign.start,);
  }
}