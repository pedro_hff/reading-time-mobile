import 'package:flutter/material.dart';
import 'package:mobile/widgets/round_button.dart';
import 'dart:async';
import 'package:mobile/widgets/custom_show_dialog.dart';
import 'package:mobile/widgets/chronometer.dart';

class ChronometerController extends StatefulWidget {
  ChronometerController({
    Key key,
    @required this.bookName,
    this.visibility,
    this.animationValue
  }): super(key: key);

  String bookName;
  bool visibility;
  double animationValue;

  @override
  _ChronometerController createState() => _ChronometerController();

}

class _ChronometerController extends State<ChronometerController> {
  RoundButton _stopButton;
  RoundButton _playPauseButton;
  final double _ROUND_BUTTON_SIZE = 28.0;
  Widget _controllerContainer;
  final Dependencies dependencies = new Dependencies();

  setBook(String bookName) {
    setState(() {
      widget.bookName = bookName;
    });
  }

  playPause() {
    setState(() {
      // Play/Pause
    });
  }

  @override
  Widget build(BuildContext context) {
    _buildPlayPauseButton();
    _buildStopButton();
    return _buildAnimatedOpacityWithContainer();
  }

  Widget _buildAnimatedOpacityWithContainer() {
    if(_controllerContainer == null) {
      _controllerContainer = _buildContainer();
    }


    return AnimatedOpacity(
      opacity: widget.visibility ? 1.0 : 0.0,
      duration: Duration(milliseconds: 300),
      child: GestureDetector(
        onTap: (){openModal();},
        child: _controllerContainer,
      ),
      curve: Curves.easeIn,
    );
  }

  Future<Null>openModal() async {
    await showDialog(
      context: context,
      builder: (_) => new CustomAlertDialog(
        content: Flex(
          direction: Axis.horizontal,
          ),
      )
    );
  }

  Widget _buildContainer() {
    return Container(
      height: 35.0,
      width: MediaQuery.of(context).size.width - 32.0,
      decoration: new BoxDecoration(
          borderRadius: new BorderRadius.all(Radius.circular(30.0)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black26,
                blurRadius: 1.0,
                spreadRadius: 1.0,
                offset: Offset(0.0, 1.0)
            )
          ]
      ),
      padding: EdgeInsets.only(left: 4.0, right: 4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Expanded(
              child: new Row(
                children: <Widget>[
                  _stopButton,

                  new Padding(padding: EdgeInsets.only(left: 3.0)),

                  new Expanded(
                    child: Container(
                      child: new Text(
                        widget.bookName,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 18.0
                        ),
                      ),
                    )
                  ),

                ],
              )
          ),

          new Padding(padding: EdgeInsets.only(left: 3.0)),

          new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 85.0,
                  child: TimerText(dependencies: dependencies),
                ),

                _playPauseButton
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _clickPlayPauseButton() {
    setState(() {
      if (dependencies.stopwatch.isRunning) {
        dependencies.stopwatch.stop();
      } else {
        dependencies.stopwatch.start();
      }
    });
  }

  void _clickStopButton() {
    // TODO: Confirm dialog to reset
    dependencies.stopwatch.stop();
    dependencies.stopwatch.reset();
  }

  void _buildPlayPauseButton() {
    _playPauseButton = RoundButton(
        icon: Icons.play_arrow,
        iconColor: Colors.white,
        buttonColor: Colors.lightGreen,
        highlightColor: Colors.green,
        size: _ROUND_BUTTON_SIZE,
        onPressed: _clickPlayPauseButton
    );
  }

  void _buildStopButton() {
    _stopButton = RoundButton(
      icon: Icons.stop,
      iconColor: Colors.white,
      buttonColor: Colors.red,
      highlightColor: Colors.black54,
      size: _ROUND_BUTTON_SIZE,
      onPressed: _clickStopButton,
    );
  }
}