import 'package:flutter/material.dart';

class RoundButton extends StatefulWidget {
  RoundButton({
    Key key,
    @required this.icon,
    @required this.iconColor,
    @required this.buttonColor,
    @required this.highlightColor,
    @required this.size,
    @required this.onPressed
  }): super(key:key);

  IconData icon;
  Color iconColor;
  Color buttonColor;
  Color highlightColor;
  double size;
  VoidCallback onPressed;

  @override
  _RoundButtonState createState() => _RoundButtonState();
}

class _RoundButtonState extends State<RoundButton> {

  IconData currentIcon;

  void setIcon(IconData icon) {
    setState(() {
      currentIcon = icon;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints(maxWidth: widget.size, maxHeight: widget.size, minHeight: widget.size, minWidth: widget.size),
      child: new FlatButton(
        color: widget.buttonColor,
        highlightColor: widget.highlightColor,
        onPressed: widget.onPressed,
        child:  Icon(widget.icon, color: widget.iconColor),
        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
        padding: EdgeInsets.all(0.0),
      ),
    );
  }

}